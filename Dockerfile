FROM ubuntu:trusty
MAINTAINER Erick Almeida <ephillipe@gmail.com>

# Install S3FS

# S3FS Version
ENV S3FS_VERSION="1.78"

# Install some deps for S3FS
WORKDIR /deploy
RUN apt-get update -qq \
    && apt-get install -y build-essential libmount1 libblkid1 libfuse-dev exfat-utils libcurl4-openssl-dev libxml2-dev mime-support automake libtool wget tar 

# Make sure we have S3
RUN wget https://github.com/s3fs-fuse/s3fs-fuse/archive/v$S3FS_VERSION.tar.gz -O /usr/src/v$S3FS_VERSION.tar.gz \
    && tar xvz -C /usr/src -f /usr/src/v$S3FS_VERSION.tar.gz \
    && cd /usr/src/s3fs-fuse-$S3FS_VERSION \
    && ./autogen.sh \
    && ./configure --prefix=/usr \
    && make \
    && make install \
    && mkdir /var/lib/odoo/filestore \
    && chmod 777 /var/lib/odoo/filestore \
    && apt-get remove -y --purge --force-yes build-essential libfuse-dev libcurl4-openssl-dev libxml2-dev mime-support automake libtool wget \
    && apt-get autoremove -y --force-yes \
    && apt-get install -y libcurl3 libxml2 \
    && rm -rf /deploy

# Install some deps, lessc and less-plugin-clean-css, and wkhtmltopdf
RUN set -x; \
        apt-get update \
        && apt-get install -y --no-install-recommends \
            ca-certificates \
            curl \
            nodejs \
            npm \
            python-support \
            python-pyinotify \
        && npm install -g less less-plugin-clean-css \
        && ln -s /usr/bin/nodejs /usr/bin/node \
        && curl -o wkhtmltox.deb -SL http://nightly.odoo.com/extra/wkhtmltox-0.12.1.2_linux-jessie-amd64.deb \
        && echo '40e8b906de658a2221b15e4e8cd82565a47d7ee8 wkhtmltox.deb' | sha1sum -c - \
        && dpkg --force-depends -i wkhtmltox.deb \
        && apt-get -y install -f --no-install-recommends \
        && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false -o APT::AutoRemove::SuggestsImportant=false npm \
        && rm -rf /var/lib/apt/lists/* wkhtmltox.deb

# Install Odoo
ENV ODOO_VERSION 8.0
ENV ODOO_RELEASE 20150401
RUN set -x; \
        curl -o odoo.deb -SL http://nightly.odoo.com/${ODOO_VERSION}/nightly/deb/odoo_${ODOO_VERSION}.${ODOO_RELEASE}_all.deb \
        && dpkg --force-depends -i odoo.deb \
        && apt-get update \
        && apt-get -y install -f --no-install-recommends \
        && rm -rf /var/lib/apt/lists/* odoo.deb

# Copy entrypoint script and Odoo configuration file
COPY ./entrypoint.sh /
COPY ./openerp-server.conf /etc/odoo/
RUN chown odoo /etc/odoo/openerp-server.conf

# Mount /var/lib/odoo to allow restoring filestore and /mnt/extra-addons for users addons
VOLUME ["/var/lib/odoo", "/mnt/extra-addons", "/var/lib/odoo/filestore"]

# Expose Odoo services
EXPOSE 8069 8071

# Set the default config file
ENV OPENERP_SERVER /etc/odoo/openerp-server.conf

# Set default user when running the container
USER odoo

ENTRYPOINT ["/entrypoint.sh"]
CMD ["openerp-server"]
